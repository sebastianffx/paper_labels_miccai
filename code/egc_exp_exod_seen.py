import pickle
import sys
sys.path.insert(0, '/opt/caffe/python/')
import caffe
import os
import pandas as pd
from sklearn.metrics import confusion_matrix, accuracy_score, make_scorer
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from os import listdir
from os.path import isfile, join
import numpy as np 
import skimage.io as io
import matplotlib.pyplot as plt
import pylab
from sklearn.metrics import confusion_matrix
from sklearn import svm
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
import json
from pprint import pprint
import scipy.io as sio
from PIL import Image
from caffe import layers as L, params as P # Shortcuts to define the net prototxt.
import os.path as osp
import random
from random import randint
caffe.set_mode_gpu()
#import display_network
#%matplotlib inline
# ecg computes which samples are more informative in the model w.r.t. to the norm of gradients
# assumes that the model is a caffe net with memory data layer
# returns the gradients and the index of the selected samples



def egc_batch(cf_model, samples, data4D, labels4D, queries, mode_egc, layer, num_active_selections):
    expected_value_samples = []
    zero_patches = np.zeros((31,3,48,48))
    zero_labels = np.zeros((31,1,1,1))
    count_smpls = 0
    num_to_check = 1000
    sub_samples = []
    samples_to_check = np.arange(len(samples))
    np.random.shuffle(samples_to_check)
    for i in range(num_to_check):
        sub_samples.append(samples[samples_to_check[i]])
    print "Toca revisar " + str(len(sub_samples))
    samples_grads = []
    for cur_sampl in sub_samples:
        if count_smpls % 100 == 0:
            print "Revisando el ejemplo " + str(count_smpls) + ", de " + str(len(sub_samples)) + " ejemplos!"
        cur_samples = np.array(np.concatenate((zero_patches, data4D[cur_sampl].reshape(1,3,48,48)), axis=0), np.float32)
        count_smpls = count_smpls + 1
        #For each sample we compute the sum of its expected gradient vals
        sum_expected_vals_cur = 0 
        for lbl in range(2):
            cur_labls = np.array(np.concatenate((zero_labels, np.array([[[[lbl]]]], np.float32)),axis=0), np.float32)
            cf_model.net.set_input_arrays(cur_samples, cur_labls)
            sum_expected_vals_cur +=cf_model.net.forward()['prob'].flatten()[lbl]*np.linalg.norm(cf_model.net.backward(diffs=['ip1'])['ip1'])
            samples_grads.append(np.linalg.norm(cf_model.net.backward(diffs=['ip1'])['ip1'])) 
        expected_value_samples.append(sum_expected_vals_cur) 
    #Now lets sort by most informative samples
    actv_smpls_values = zip(samples, expected_value_samples)
    actv_smpls_values = sorted(actv_smpls_values, key=lambda x: x[1])
    active_samples_updt_params = [actv_smpls_values[-k][0] for k in range(1,num_active_selections)] #just the k most informative samples
    return [active_samples_updt_params, actv_smpls_values]



def egc(cf_model, samples, data4D, labels4D, queries, mode_egc, layer, num_active_selections):
    expected_value_samples = []
    samples_grads = []
    for cur_sampl in samples:
        #For each sample we compute the sum of its expected gradient vals
        sum_expected_vals_cur = 0 
        for lbl in range(2):
            cf_model.net.set_input_arrays(data4D[cur_sampl].reshape(1,3,48,48), np.array([[[[lbl]]]], np.float32))
            sum_expected_vals_cur +=cf_model.net.forward()['prob'].flatten()[lbl]*np.linalg.norm(cf_model.net.backward(diffs=['ip1'])['ip1'])
            samples_grads.append(np.linalg.norm(cf_model.net.backward(diffs=['ip1'])['ip1'])) 
        expected_value_samples.append(sum_expected_vals_cur) 
    #Now lets sort by most informative samples
    actv_smpls_values = zip(samples, expected_value_samples)
    actv_smpls_values = sorted(actv_smpls_values, key=lambda x: x[1])
    active_samples_updt_params = [actv_smpls_values[-k][0] for k in range(1,num_active_selections)] #just the k most informative samples
    return [active_samples_updt_params, actv_smpls_values]


def loadSamples(path_part):
    labels = os.listdir(path_part)
    dataset = np.empty((0,2))

    for label in labels:
        samples = sorted(os.listdir(os.path.join(path_part, label)))
        for sample in samples:
            path = os.path.join(path_part, label, sample)
            if os.path.isfile(path):
                dataset = np.concatenate((dataset, np.array([[label, path]])), axis=0)
    return dataset






paths_val_file = '/home/ojperdomoc/Parches48/Modelo_4/Validation/'
paths_train_file = '/home/ojperdomoc/Parches48/Modelo_4/Training/'
paths_test_file = '/home/ojperdomoc/Parches48/Modelo_4/Test/'

dataset_paths_train = loadSamples(paths_train_file)
dataset_paths_val = loadSamples(paths_val_file)
dataset_paths_test = loadSamples(paths_test_file)
i=0
mean_root = '/home/ojperdomoc/Predict_mask/Modelo1/'


#Load image from Newmodel (MemoryData)
#init_solver_path = '/home/jsotaloram/active_learning/labels_miccai/.prototxt'
init_solver_path = '/home/jsotaloram/active_learning/labels_miccai/solver_modelo_batch.prototxt'

init_solver = caffe.SGDSolver(init_solver_path)

transformer = caffe.io.Transformer({'data': init_solver.net.blobs['data'].data.shape})
transformer.set_transpose('data', (2,0,1))
transformer.set_mean('data', np.load(mean_root+'mean4.npy').mean(1).mean(1)) # mean pixel
transformer.set_raw_scale('data', 255)  # the reference model operates on images in [0,255] range instead of [0,1]
transformer.set_channel_swap('data', (2,1,0))  # the reference model has channels in BGR order instead of RGB
trans_patch = transformer.preprocess('data', caffe.io.load_image(dataset_paths_train[i][1]))



#loading data
train_size = len(dataset_paths_train)
val_size = len(dataset_paths_val)
test_size = len(dataset_paths_test)


data4D = np.zeros([train_size+48,3,48,48],np.float32)
data4DL = np.zeros([train_size+48,1,1,1],np.float32)
data4D_val = np.zeros([val_size,3,48,48],np.float32)
data4DL_val = np.zeros([val_size,1,1,1],np.float32)
data4D_test = np.zeros([test_size+24,3,48,48],np.float32)
data4DL_test = np.zeros([test_size+24,1,1,1],np.float32)


for i in range(len(dataset_paths_train)+48):
    if i<len(dataset_paths_train):
        #Substracting the mean, swaping axes, et al
        trans_patch = transformer.preprocess('data', caffe.io.load_image(dataset_paths_train[i][1]))
        data4D[i] = trans_patch
        data4DL[i,:,:,:] = np.array(dataset_paths_train[i][0])
    else:
        data4D[i,:,:,:] = data4D[0,:,:,:]
        data4DL[i,:,:,:] = data4DL[0,:,:,:]


for i in range(len(dataset_paths_val)):
    trans_patch = transformer.preprocess('data', caffe.io.load_image(dataset_paths_val[i][1]))
    data4D_val[i] = trans_patch
    data4DL_val[i,:,:,:] = np.array(dataset_paths_val[i][0])

for i in range(len(dataset_paths_test)+24):
    if i<len(dataset_paths_test):
        trans_patch = transformer.preprocess('data', caffe.io.load_image(dataset_paths_test[i][1]))
        data4D_test[i] = trans_patch
        data4DL_test[i,:,:,:] = np.array(dataset_paths_test[i][0])
    else:
        data4D_test[i,:,:,:] = data4D_test[0,:,:,:]
        data4DL_test[i,:,:,:] = data4DL_test[0,:,:,:]

bin_ex_labels_val = []
for i in range(data4DL_val.shape[0]):
    if np.sum(data4DL_val[i].flatten())>0:
        bin_ex_labels_val.append(1)
    else:
        bin_ex_labels_val.append(0)
bin_ex_labels_test = []
for i in range(data4DL_test.shape[0]):
    if np.sum(data4DL_test[i].flatten())>0:
        bin_ex_labels_test.append(1)
    else:
        bin_ex_labels_test.append(0)


print "Data loaded! === Begining of Active Selection Net Training"

print "Train samples: " + str(len(dataset_paths_train))
print "Validation samples: " + str(len(dataset_paths_val))
print "Test samples: " + str(len(dataset_paths_test))

print dataset_paths_train[0]
print dataset_paths_val[0]
print dataset_paths_test[0]
all_egc_measures = []
reports = []
confusion_matrices = []
remain_samples = np.arange(data4D.shape[0])
np.random.shuffle(remain_samples)
seen_samples_rs = []
all_gradients = []


num_its_cvg = 2
num_init_queries = 1000
num_active_selections = 32


train_errors = np.zeros(num_init_queries)


all_samples_seen_al_seen = []
test_net = init_solver.test_nets[0]
test_net.set_input_arrays(np.expand_dims(data4D_val[0],axis=0), np.array([[[[1]]]], np.float32))
#test_net.set_input_arrays(data4D_test,data4DL_test)                                                     
solver_path = '/home/jsotaloram/active_learning/labels_miccai/solver_modelo1.prototxt'
solver_1sizedbatch = caffe.SGDSolver(solver_path)

for rand_it in range(1):
    measures = []
    for it in range(num_init_queries):
        pred_labels_test_ex = []
        cur_samples = []

        #solver_1sizedbatch.net.copy_from('/home/jsotaloram/active_learning/labels_miccai/models/model_batch_tmp.caffemodel')
        print "Empece a revisar cuales ejemplos son los mejores! "
        [active_samples_updt_params, actv_smpls_values] = egc_batch(init_solver,remain_samples,data4D,'','','','',num_active_selections+1)
        print "Finalice de revisar cuales ejemplos son los mejores! "

        for i in range(len(active_samples_updt_params)):
            cur_sumple = active_samples_updt_params[i]
            all_samples_seen_al_seen.append(cur_sumple)
            cur_samples.append(cur_sumple)
        all_gradients.append(actv_smpls_values)
        remain_samples = [x for x in remain_samples if x not in cur_samples]
        
        
        #Choosing a shuffled order for retraining with seen samples as prof. Fabio suggested
        sampls_order = np.arange(len(all_samples_seen_al_seen))
        np.random.shuffle(sampls_order)
        new_ordered_all_samples = [all_samples_seen_al_seen[i] for i in sampls_order]
        all_batches_seen = [new_ordered_all_samples[i:i+32] for i in xrange(0, len(new_ordered_all_samples), 32)]
        
            
        for batch in all_batches_seen:    
            for cvg_it in range(num_its_cvg):
                init_solver.net.set_input_arrays(data4D[batch], data4DL[batch])
                init_solver.step(1)

        train_errors[it] =  init_solver.net.blobs['loss'].data

        #for act_sel_sample in cur_samples:#[cur_selected_act_sample]:
        #     np.delete(remain_samples,act_sel_sample)
#Evaluation code                                                                                                     
        for j in range(data4D_test.shape[0]):
            test_net.set_input_arrays(np.expand_dims(data4D_test[j], axis=0),  np.array([data4DL_test[j]], np.float32))
            output = test_net.forward()
            pred_labels_test_ex.append(test_net.blobs['prob'].data.flatten().argsort()[-1:-2:-1][0])
       
        class_report = classification_report(bin_ex_labels_test, pred_labels_test_ex)
        reports.append(class_report)
        cr_f1 = class_report.split()
        cfm = confusion_matrix(bin_ex_labels_test, pred_labels_test_ex)
        confusion_matrices.append(cfm)
        if ((float(cfm[0,0]) + float(cfm[1,0]))!=0 and (float(cfm[1,1]) + float(cfm[0,1]))!=0):
            sensitivity = float(cfm[0,0])/(float(cfm[0,0]) + float(cfm[1,0]))
            specificity = float(cfm[1,1])/(float(cfm[1,1]) + float(cfm[0,1]))
            measures.append((class_report,cfm,sensitivity,specificity,train_errors[it]))
        #if it%10 == 0:
            
            #init_solver.net.save('/home/jsotaloram/active_learning/labels_miccai/results/')
        with open('/home/jsotaloram/active_learning/labels_miccai/results/measures_egc_testBATCH_it'+str(rand_it)+'.pkl','w') as f:
            pickle.dump(measures,f)
        print 'Current smpl: ' + str(cur_sumple) + '  with label: '+str(np.array([data4DL[cur_sumple]]))+', TrainERR: ' + str(train_errors[it]) + ', Samples seen: ' + str(len(all_samples_seen_al_seen)) +', F1, so far:' + str(cr_f1[-2]) + ", Sensitivity: " + str(sensitivity), ", Specificity: " + str(specificity)
    all_egc_measures.append(measures)



