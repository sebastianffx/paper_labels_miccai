import numbers
from numpy.lib.stride_tricks import as_strided
import collections
import pickle
import sys
sys.path.insert(0, '/opt/caffe/python/')
import caffe
import os
import pandas as pd
from sklearn.metrics import confusion_matrix, accuracy_score, make_scorer
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from os import listdir
from os.path import isfile, join
import numpy as np
import skimage.io as io
import matplotlib.pyplot as plt
import pylab
from sklearn.metrics import confusion_matrix
from sklearn import svm
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
import json
from pprint import pprint
import scipy.io as sio
from PIL import Image
from caffe import layers as L, params as P # Shortcuts to define the net prototxt.                  
import matplotlib.pyplot as plt
import matplotlib.cm as cm

import os.path as osp
import random
from random import randint
caffe.set_mode_gpu()
def crop_image(input_image, output_image, start_x, start_y, widtha, heighta,mem=True):
    """Pass input name image, output name image, x coordinate to start croping, y coordinate to start croping, width to crop, height to crop """
    widtha=47
    heighta=47
    sep="_"
    bord="_0"
    input_img = Image.open(input_image)
    box = (start_x, start_y, start_x + widtha, start_y + heighta)
    output_img = input_img.crop(box)
    
    if mem:
        return np.array(output_img)
    else:
        output_img.save(output_image + str(start_x) + str(sep) + str(start_y) + str(bord) +".jpg")  
        
        
def evaluate_test_image(testimgpath, outpath):
    #Loading the test image
    original = Image.open(testimgpath)
    #original.show()
    #plt.imshow(original)
    width, height = original.size
    limit_w=width/48
    limit_h=height/48
    left = width/4
    top = height/4
    right = 3 * width/4
    bottom = 3 * height/4
    out_dir = outpath+testimgpath.split('/')[-1].split('.')[0]+'/patches/'
    out_patches = []
    #Creating patches to load on memory and create the heatmap 
    #print "the output dir is " + out_dir  
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    for i in range(0, width, 47):
        for j in range(0,height, 47):
    #Create (width-31)*(height-31) images (1'308.961)
            a= crop_image(testimgpath,out_dir+"output", i, j, 47+i, 47+j, True)
            out_patches.append(a)
            #print i, j
    print "Test patches created"
    
    #TODO also keep them in memory!, there is no necessity to save them, that could be an overhea
    return out_patches


def create_4D_caffe_tensor_from_mem(patches):
    caffe4D = np.zeros((patches.shape))
    return caffe4D




def extract_patches(arr, patch_shape=8, extraction_step=1):
    """Extracts patches of any n-dimensional array in place using strides.
    Given an n-dimensional array it will return a 2n-dimensional array with
    the first n dimensions indexing patch position and the last n indexing
    the patch content. This operation is immediate (O(1)). A reshape
    performed on the first n dimensions will cause numpy to copy data, leading
    to a list of extracted patches.
    Read more in the :ref:`User Guide <image_feature_extraction>`.
    Parameters
    ----------
    arr : ndarray
        n-dimensional array of which patches are to be extracted
    patch_shape : integer or tuple of length arr.ndim
        Indicates the shape of the patches to be extracted. If an
        integer is given, the shape will be a hypercube of
        sidelength given by its value.
    extraction_step : integer or tuple of length arr.ndim
        Indicates step size at which extraction shall be performed.
        If integer is given, then the step is uniform in all dimensions.
    Returns
    -------
    patches : strided ndarray
        2n-dimensional array indexing patches on first n dimensions and
        containing patches on the last n dimensions. These dimensions
        are fake, but this way no data is copied. A simple reshape invokes
        a copying operation to obtain a list of patches:
        result.reshape([-1] + list(patch_shape))
    """

    arr_ndim = arr.ndim

    if isinstance(patch_shape, numbers.Number):
        patch_shape = tuple([patch_shape] * arr_ndim)
    if isinstance(extraction_step, numbers.Number):
        extraction_step = tuple([extraction_step] * arr_ndim)

    patch_strides = arr.strides

    slices = [slice(None, None, st) for st in extraction_step]
    indexing_strides = arr[slices].strides

    patch_indices_shape = ((np.array(arr.shape) - np.array(patch_shape)) //
                           np.array(extraction_step)) + 1

    shape = tuple(list(patch_indices_shape) + list(patch_shape))
    strides = tuple(list(indexing_strides) + list(patch_strides))

    patches = as_strided(arr, shape=shape, strides=strides)
    return patches, indexing_strides, patch_indices_shape, slices



def compute_n_patches(i_h, i_w, p_h, p_w, step, max_patches=None):
    """   
    Compute the number of patches that will be extracted in an image.
    
    Parameters
    ----------
    i_h : int
        The image height

    i_w : int
        The image with

    p_h : int
        The height of a patch

    p_w : int
        The width of a patch

    max_patches : integer or float, optional default is None
        The maximum number of patches to extract. If max_patches is a float
        between 0 and 1, it is taken to be a proportion of the total number
        of patches.

    Returns
    -------
    all_patches
        Description.
    """
    n_h = (i_h - p_h)/step + 1
    n_w = (i_w - p_w)/step + 1
    all_patches = n_h * n_w

    if max_patches:
        if (isinstance(max_patches, (numbers.Integral))
                and max_patches < all_patches):
            return max_patches
        elif (isinstance(max_patches, (numbers.Real))
                and 0 < max_patches < 1):
            return int(max_patches * all_patches)
        else:
            raise ValueError("Invalid value for max_patches: %r" % max_patches)
    else:
        return all_patches

def compute_corner_patches(img_size, stride_x, stride_y):
    array_corners = []
    total_p_x,total_p_y  = 0,0
#    for i in range(0,img_size[0]-5*stride_x,stride_x): #Works for 48x48, 8 stride patches
    stride_x_corner = 5
    stride_y_corner = 5
    for i in range(0,img_size[0]-stride_x_corner*stride_x,stride_x):
        total_p_x = total_p_x +1
        corners_row = []
#        for j in range(0,img_size[1]-5*stride_y, stride_y):
        for j in range(0,img_size[1]-stride_y_corner*stride_y, stride_y):
            if i == 0:
                total_p_y = total_p_y +1
            corners_row.append((i,j))
        array_corners.append(corners_row)
    return np.array(array_corners),total_p_x,total_p_y


def reconstruct_from_patches_2d(patches, image_size):
    """Reconstruct the image from all of its patches.
    Patches are assumed to overlap and the image is constructed by filling in
    the patches from left to right, top to bottom, averaging the overlapping
    regions.
    Read more in the :ref:`User Guide <image_feature_extraction>`.
    Parameters
    ----------
    patches : array, shape = (n_patches, patch_height, patch_width) or
        (n_patches, patch_height, patch_width, n_channels)
        The complete set of patches. If the patches contain colour information,
        channels are indexed along the last dimension: RGB patches would
        have `n_channels=3`.
    image_size : tuple of ints (image_height, image_width) or
        (image_height, image_width, n_channels)
        the size of the image that will be reconstructed
    Returns
    -------
    image : array, shape = image_size
        the reconstructed image
    """
    i_h, i_w = image_size[:2]
    print i_h, i_w
    p_h, p_w =  patches[0,0].shape[0], patches[0,0].shape[1]#patches.shape[1:3]
    print p_h, p_w
    img = np.zeros(image_size)
    #print img.shape
    # compute the dimensions of the patches array
    n_h = i_h - p_h + 1
    print n_h
    n_w = i_w - p_w + 1
    print n_w
    pp  = patches.reshape(patches.shape[0]*patches.shape[1], patches.shape[2], patches.shape[3])
    for p,  i in zip(pp, range(n_h)):
        for p, j in zip(pp, range(n_w)):
            img[i:i + p_h, j:j + p_w] += p
    #for p, (i, j) in zip(pp, product(range(n_h), range(n_w))):
     #   print (i,j)
        #img[i:i + p_h, j:j + p_w] += p
        #print patches[i,j].shape
    #  img[i:i + p_h, j:j + p_w] += p
    #for i in range(n_h):
        #for j in range(n_w):
            #print patches[i,j]
            #img[i:i + p_h, j:j + p_w] += patches[i,j] 
    
    for i in range(i_h):
        for j in range(i_w):
            # divide by the amount of overlap
            # XXX: is this the most efficient way? memory-wise yes, cpu wise?
            img[i, j] /= float(min(i + 1, p_h, i_h - i) *
                               min(j + 1, p_w, i_w - j))
    return img

#This reconstruct a 2D image of size=img_size from patches extracted using extract_patches and coordinates array from compute_corner_patches

def reconstruct_img_from_patchified(patches, array_corners, img_size, patch_size):
    rec_img = np.zeros(img_size)
    corners_dict = {}
    for i in range(array_corners.shape[0]):
        for j in range (array_corners.shape[1]):
            corners_dict[str(array_corners[i,j][0])+'_'+str(array_corners[i,j][1])] = (i,j)
    cKeys = corners_dict.keys()
    for corner in cKeys:
        xc = int(corner.split('_')[0])
        xy = int(corner.split('_')[1])
        rec_img[xc:xc+patch_size,xy:xy+patch_size] = rec_img[xc:xc+patch_size,xy:xy+patch_size] + patches[corners_dict[corner][0],corners_dict[corner][1]]
    p_h,p_w = patch_size,patch_size
#    for i in range(img_size[0]):
#        for j in range(img_size[1]):
            # divide by the amount of overlap
            # XXX: is this the most efficient way? memory-wise yes, cpu wise?
#            rec_img[i, j] /= float(min(i + 1, p_h, img_size[0] - i) *
#                               min(j + 1, p_w, img_size[1] - j))
    return rec_img


#This reconstruct a 2D image of size=img_size from patches extracted using extract_patches and coordinates array from compute_corner_patches
def reconstruct_img_from_patchified_px(patches, array_corners, img_size, patch_size):
    rec_img = np.zeros(img_size)
    corners_dict = {}
    for i in range(array_corners.shape[0]):
        for j in range (array_corners.shape[1]):
            corners_dict[str(array_corners[i,j][0])+'_'+str(array_corners[i,j][1])] = (i,j)
    cKeys = corners_dict.keys()
    for i in range(img_size[0]):
        for j in range(img_size[1]):
            key_cDict = str(i)+'_'+str(j)
            if key_cDict in cKeys:                
                rec_img[i:i+patch_size,j:j+patch_size] = rec_img[i:i+patch_size,j:j+patch_size] + patches[corners_dict[key_cDict][0],corners_dict[key_cDict][1]]
    return rec_img
